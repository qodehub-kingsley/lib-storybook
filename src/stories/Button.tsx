import React from 'react';
import './button.css';
// Component imports;
import './index.css';
import Grid from "../Components/Grid";
import { default as ButtonComponent } from "../Components/Button";
import * as Icon from '../Icons/index';

export interface ButtonProps {
  /**
   * Is the button submitting data?
   */
  isSubmitting?: boolean;
  /**
   * What color to use when button is loading?
   */
  loadingColor?: string;
  /**
   * Button contents
   */
  children?: any;
  /**
   * Is button active or disabled?
   */
  isValid?: boolean;
  /**
   * Does button have an outline?
   */
  outline?: boolean;
  /**
   * What contents are in the button when not submitting data?
   */
  round?: boolean;
  /**
   * Optional click handler
   */
  onClick?: () => void;
}

/**
 * Primary UI component for user interaction
 */
export const Button: React.FC<ButtonProps> = ({
  isSubmitting = false,
  loadingColor = '#fff',
  children = 'Button',
  isValid = true,
  outline = false,
  round = false,
  ...props
}) => {
  return (
    <>
      <Grid sm="repeat(3, 1fr)">

        <ButtonComponent> Button </ButtonComponent>

        <ButtonComponent isValid={false}>
          <Icon.ChevronLeft />
          <span className="ml-1">Button</span>
        </ButtonComponent>
        <ButtonComponent isSubmitting>Button</ButtonComponent>

        <ButtonComponent outline>Button</ButtonComponent>
        <ButtonComponent outline isValid={false}>
          <Icon.ChevronLeft />
          <span className="ml-1">Button</span>
        </ButtonComponent>
        <ButtonComponent isSubmitting outline loadingColor="var(--chateau-green)">
          Button
        </ButtonComponent>

        <ButtonComponent isValid round>
          <Icon.ArrowRight />
        </ButtonComponent>
        <ButtonComponent round isValid={false}>
          <Icon.ArrowRight />
        </ButtonComponent>
        <ButtonComponent isSubmitting round>
          <Icon.ArrowRight />
        </ButtonComponent>

        <ButtonComponent isValid round outline>
          <Icon.ArrowRight />
        </ButtonComponent>
        <ButtonComponent round outline isValid={false}>
          <Icon.ArrowRight />
        </ButtonComponent>
        <ButtonComponent
          isSubmitting
          round
          outline
          loadingColor="var(--chateau-green)"
        >
          <Icon.ArrowRight />
        </ButtonComponent>
      </Grid>
    </>
  );
};