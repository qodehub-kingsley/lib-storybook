import React from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';
import Instructions from "../Components/Instructions";

export default {
  title: 'Instruction',
  component: Instructions,
} as Meta;

const Template: Story<{}> = 
(args) => 
  <Instructions
  {...args}
    instructions={ [args.instructions='First Instruction', 'Second Instruction'] || [
      "Only Ghanaian nationals can apply for national service.",
      "Graduates from Ghanaian universities MUST have graduated at least 1 year prior to the service year they seek to join.",
      "All current year graduates can only enroll for national service through their institution of study.",
      "International graduates must have fully completed their studies, with results published to enroll for NSS.",
    ]}
/>


export const Props = Template.bind({});
// Props.args = {
  
// };
