import React, { useState } from "react";
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';
import Button from "../Components/Button";
import Confirm from "../Components/Confirm";
import Notify from "../Components/Notify";
import Modal from "react-bootstrap/Modal";
import Dropdown from "react-bootstrap/Dropdown";
import { toast } from "react-toastify";
import * as Icon from '../Icons/index';
// CSS abd stylesheets
import './index.css';

export default {
  title: 'Modal and Confirmation',
  component: Modal,
  argTypes: {
    // backgroundColor: { control: 'color' },
  },
} as Meta;

const ModalConfirmation: Story<{}> = (args) => {
  const [modal, setModal] = useState(false);

  const handleToast = (type, title) => {
    return toast(
      <Notify
        body="Hello world. This is a toast"
        {...(title && { title: title })}
        type={type}
        {...args}
      />,
    );
  };

  return (
    // <Modal {...args}>Hello world</Modal>
    <>
      <div className='displayAsGrid2'> 
        <Button {...args} onClick={() => setModal(true)} className="mb-8">
          Open Modal
        </Button>
        <Button
          {...args}
          onClick={async () => {
            if (
              await Confirm({ msg: "Hello world", header: "Hello world" })
            ) {
            }
          }}
          className="mb-8"
        >
          Open Confirm
        </Button>
      </div>
      <div className='displayAsGrid2'>
          
        <Button
          {...args}
          onClick={() => toast(<Notify body="Hello world" type="error" />)}
          className="mb-8"
        >
          Open Toast
        </Button>

        <Dropdown>
          <Dropdown.Toggle className="btn--default">
            <span className="mr-2">Open toast</span>
            <Icon.ChevronDown />
          </Dropdown.Toggle>
          <Dropdown.Menu>
            <Dropdown.Item onClick={() => handleToast("success")}>
              Success toast
            </Dropdown.Item>
            <Dropdown.Item onClick={() => handleToast("error")}>
              Error toast
            </Dropdown.Item>
            <Dropdown.Item onClick={() => handleToast(null)}>
              Default toast
            </Dropdown.Item>
            <Dropdown.Item onClick={() => handleToast(null, "Header")}>
              With header
            </Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>

        <Modal show={modal} onHide={() => setModal(false)} centered {...args}>
          <Modal.Body>
            <p className="text-body">Hello world</p>
          </Modal.Body>
        </Modal>
      </div>
    </>
  )
} 

const ModalandConfirmation: Story<{}> = (args) => {
  const [modal, setModal] = useState(false);

  const handleToast = (type, title) => {
    return toast(
      <Notify
        body="Hello world. This is a toast"
        {...(title && { title: title })}
        type={type}
      />,
    );
  };

  return (
    <div className='displayAsGrid2'> 
      
      <Button {...args} onClick={() => setModal(true)} className="mb-8">
        Open Modal
      </Button>

      <Button
        {...args}
        onClick={async () => {
          if (
            await Confirm({ msg: "Hello world", header: "Hello world" })
          ) {
          }
        }}
        className="mb-8"
      >
        Open Confirm
      </Button>

      <Modal {...args} show={modal} onHide={() => setModal(false)} centered>
        <Modal.Body>
          <p className="text-body">Hello world</p>
        </Modal.Body>
      </Modal>
    </div>
  )
} 

const OpenToast: Story<{}> = (args) => {
  const [modal, setModal] = useState(false);

  const handleToast = (type, title) => {
    return toast(
      <Notify
        body="Hello world. This is a toast"
        {...(title && { title: title })}
        type={type}
      />,
    );
  };

  return (
    <div className='displayAsGrid2'> 
      
      <Button
        {...args}
        onClick={() => toast(<Notify body="Hello world" type="error" />)}
        className="mb-8"
      >
        Open Toast
      </Button>

      <Dropdown>
        <Dropdown.Toggle className="btn--default">
          <span className="mr-2">Open toast</span>
          <Icon.ChevronDown />
        </Dropdown.Toggle>
        <Dropdown.Menu>
          <Dropdown.Item onClick={() => handleToast("success")}>
           Success toast
         </Dropdown.Item>
          {/*
           <Dropdown.Item onClick={() => handleToast("success")}>
            Success toast
          </Dropdown.Item>
          <Dropdown.Item onClick={() => handleToast("error")}>
            Error toast
          </Dropdown.Item>
          <Dropdown.Item onClick={() => handleToast(null)}>
            Default toast
          </Dropdown.Item> */}
          <Dropdown.Item onClick={() => handleToast(null, "Header")}>
            With header
          </Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>

      <Modal {...args} show={modal} onHide={() => setModal(false)} centered>
        <Modal.Body>
          <p className="text-body">Hello world</p>
        </Modal.Body>
      </Modal>
    </div>
  )
} 

OpenToast.storyName='I am the primary';


// const AlertSuccess: Story<{}> = (args) => 
//   <div className='displayAsBlock'> 
    
//     <Alert variant="success">Hello world</Alert>
//     <Alert variant="success">
//       <Icon.Info variant="fill" />
//       Hello world
//     </Alert>
//     <Alert variant="success" withBorder>
//       <Icon.Info variant="fill" />
//       Hello world
//     </Alert>
//   </div>;

// const AlertWarning: Story<{}> = (args) => {
//   return (
//     <div className='displayAsBlock'>
        
//         <Alert variant="warning">Hello world</Alert>
//         <Alert variant="warning">
//           <Icon.Info variant="fill" />
//           Hello world
//         </Alert>
//         <Alert variant="warning" withBorder>
//           <Icon.Info variant="fill" />
//           Hello world
//         </Alert>
//     </div>
//   );
// };

// const AlertDanger: Story<{}> = (args) => {
//   return (
//     <div className='displayAsBlock'>
//       <Alert variant="danger">Hello world</Alert>
//       <Alert variant="danger">
//         <Icon.Info variant="fill" />
//         Hello world
//       </Alert>
//       <Alert variant="danger" withBorder>
//         <Icon.Info variant="fill" />
//         Hello world
//       </Alert>
//     </div>
//   );
// };

// const AlertColor: Story<{}> = (args) => {
//   return (
//     <div className='displayAsBlock'>
//       Specify color value for a custom background color for the alert: 
//       &lt;Alert color='purple' &gt;
//       <Alert color="purple">Hello world</Alert>
//       <Alert color="purple">
//         <Icon.Info variant="fill" />
//         Hello world
//       </Alert>
//       <Alert color="purple" withBorder>
//         <Icon.Info variant="fill" />
//         Hello world
//       </Alert>
//     </div>
//   );
// };

export const Props = ModalConfirmation.bind({});
// Props.args = {
//   color: 'ColorName',
//   withBorder: false,
//   children: 'Any content',
//   variant: 'primary',
// };

export const ToastProps = OpenToast.bind({});
Props.args = {
  title: 'ColorName',
  body: 'Text to display in the notify',
  type: 'Notifyer type',
};

export const Modal_and_Confirmation = ModalandConfirmation.bind({});
export const Toast = OpenToast.bind({});
// export const Warning = AlertWarning.bind({});
// export const Danger = AlertDanger.bind({});
// export const Colorvalue = AlertColor.bind({});
