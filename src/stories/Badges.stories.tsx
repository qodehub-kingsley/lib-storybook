import React from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';
import Badge from "../Components/Badge";

export default {
  title: 'Badges',
  component: Badge,
} as Meta;

const Template: Story<{}> = (args) => {
  return (
    <div className="d-flex">
      <Badge {...args} variant={ args.variant || "success"}>
        {args.children || 'Success'}
      </Badge>
    </div>
  )
} 

const PrimaryBadge: Story<{}> = (args) => {
  return (
    <div className='badge'> 
      <Badge {...args} variant="failed">
        Failed
      </Badge>
      <Badge {...args} variant="success">
        Success
      </Badge>
      <Badge {...args} variant="processing">
        Processing
      </Badge>
    </div>
  )
} 
const CustomBadge: Story<{}> = (args) => {
  return (
    <>
      {
        /* <p>
          <code className="font-weight-700">
            &lt;Badge color='textColor' bg='backgroundColor' &gt;
          </code> - Specify custom textcolor:color and backgroundColor values:
        </p> */
      }
      <div className='badge'> 
        <Badge bg="var(--casablanca)">
          Warning
        </Badge>
        <Badge bg="var(--gull-grey)">
          Unknown
        </Badge>
        <Badge
          bg="var(--gin)"
          color="var(--black-pearl)"
        >
          Processing
        </Badge>
      </div>
    </>
  )
} 

export const Props = Template.bind({});
// Props.args = {
  
// };

export const Default_Badges = PrimaryBadge.bind({});
export const Custom_Badges = CustomBadge.bind({});