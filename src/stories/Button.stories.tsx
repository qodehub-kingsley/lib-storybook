import React from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';
import './button.css';
import Button from "../Components/Button";
import * as Icon from '../Icons/index'
import { ButtonProps } from './Button';

export default {
  title: 'Button Component',
  component: Button,
  argTypes: {
    // backgroundColor: { control: 'color' },
  },
} as Meta;

const Template: Story<ButtonProps> = (args) => <Button {...args} />;

const PrimaryButton: Story<ButtonProps> = (args) => {
  return (
    <div className='displayAsGrid'> 
      <Button> 
        Button 
      </Button>

      <Button isValid={false}>
        <Icon.ChevronLeft />
        <span className="ml-1">Button</span>
      </Button>

      <Button isSubmitting>Button</Button>
    </div>
  )
} 

const RoundButton: Story<ButtonProps> = (args) => 
  <div className='displayAsGrid'> 
    <Button isValid round>
      <Icon.ArrowRight />
    </Button>
    <Button round isValid={false}>
      <Icon.ArrowRight />
    </Button>
    <Button isSubmitting round>
      <Icon.ArrowRight />
    </Button>
  </div>;

const OutlineButton: Story<ButtonProps> = (args) => {
  return (
    <div className='displayAsGrid'>
        {/* First Button */}
        <Button outline>Button</Button>
        
        {/* Second Button */}
        <Button outline isValid={false}>
          <Icon.ChevronLeft />
          <span className="ml-1">Button</span>
        </Button>

        {/* Third Button */}
        <Button isSubmitting outline loadingColor="var(--chateau-green)">
          Button
        </Button>
    </div>
  );
};

const RoundOutlineButton: Story<ButtonProps> = (args) => {
  return (
    <div className='displayAsGrid'>
        {/* First Button */}
        <Button isValid round outline>
          <Icon.ArrowRight />
        </Button>

        {/* Second Button */}
        <Button round outline isValid={false}>
          <Icon.ArrowRight />
        </Button>
        
        {/* Third Button */}
        <Button
          isSubmitting
          round
          outline
          loadingColor="var(--chateau-green)"
        >
          <Icon.ArrowRight />
        </Button>
    </div>
  );
};

export const Props = Template.bind({});
Props.args = {
  isSubmitting: false,
  loadingColor: '#fff',
  children: 'Button',
  isValid: true,
  outline: false,
  round: false,
};

export const Primary = PrimaryButton.bind({});
Primary.args = {
  isSubmitting: false,
  loadingColor: '#fff',
  children: 'Button',
  isValid: true,
  outline: false,
  round: false,
};

export const Outline = OutlineButton.bind({});
Outline.args = {
  outline: true,
  isValid: true,
  isSubmitting: false,
  loadingColor: '#fff',
  children: 'Button',
  round: false,
};

export const Round = RoundButton.bind({});
Round.args = {
  isValid: true,
  round: true,
  outline: false,
  isSubmitting: false,
  loadingColor: '#fff',
  children: 'Button',
};

export const Round_Outline = RoundOutlineButton.bind({});
Round_Outline.args = {
  isValid: true,
  round: true,
  outline: true,
};
