import React from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';
import Upload from "../Components/Upload";
import * as Icon from '../Icons/index'

export default {
  title: 'Uploads',
  component: Upload,
} as Meta;

const Template: Story<{}> = (args) => {
  return (
    <>
      <div className='displayAsGrid2 mb-5'>
        <Upload
          setFieldValue={(file) => console.log(file)}
          label="Upload document"
          accept="image/*"
          value=""
          max={2}
          {...args}
        />
        <Upload
          tip="Some little information on what other document specifically entails"
          setFieldValue={(file) => console.log(file)}
          label="Upload document"
          accept="image/*"
          value=""
          max={2}
          {...args}
        />
      </div>
      <div className='displayAsGrid2 mb-5'>
        <Upload
          value={{ name: "CTU - Derrick.pdf" }}
          label="Upload document"
          {...args}
        />
        <Upload
          value={{ name: "CTU - Derrick Ablorh Transcript.pdf" }}
          setFieldValue={(file) => console.log(file)}
          label="Upload document"
          accept="image/*"
          withClear
          max={2}
          {...args}
        />
      </div>
    </>
  );
};

const SelectFile: Story<{}> = (args) => {
  return (
    <div className='displayAsGrid2'> 
      <Upload
        setFieldValue={(file) => console.log(file)}
        label="Upload document"
        accept="image/*"
        value=""
        max={2}
        {...args}
      />
      <Upload
        tip="Some little information on what other document specifically entails"
        setFieldValue={(file) => console.log(file)}
        label="Upload document"
        accept="image/*"
        value=""
        max={2}
        {...args}
      />
    </div>
  );
};

const UploadSuccess: Story<{}> = (args) => 
  <div className='displayAsGrid2'> 
    <Upload
      value={{ name: "CTU - Derrick.pdf" }}
      label="Upload document"
      {...args}
    />
    <Upload
      value={{ name: "CTU - Derrick Ablorh Transcript.pdf" }}
      setFieldValue={(file) => console.log(file)}
      label="Upload document"
      accept="image/*"
      withClear
      max={2}
      {...args}
    />
  </div>;

export const Props = Template.bind({});
// Props.args = {
//   color: 'ColorName',
//   withBorder: false,
//   children: 'Any content',
//   variant: 'primary',
// };

export const Upload_File = SelectFile.bind({});
export const Success = UploadSuccess.bind({});