import React from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';
import Alert from "../Components/Alert";
import * as Icon from '../Icons/index'

export default {
  title: 'Alert Component',
  component: Alert,
  argTypes: {
    // backgroundColor: { control: 'color' },
  },
} as Meta;

const Template: Story<{}> = (args) => <Alert variant="primary" {...args}>Hello world</Alert>;

const PrimaryButton: Story<{}> = (args) => {
  return (
    <div className='displayAsBlock'> 
      <Alert variant="primary">Hello world</Alert>
      <Alert variant="primary">
        <Icon.Info variant="fill" />
        Hello world
      </Alert>
      <Alert variant="primary" withBorder>
        <Icon.Info variant="fill" />
        Hello world
    </Alert>
    </div>
  )
} 

const AlertSuccess: Story<{}> = (args) => 
  <div className='displayAsBlock'> 
    
    <Alert variant="success">Hello world</Alert>
    <Alert variant="success">
      <Icon.Info variant="fill" />
      Hello world
    </Alert>
    <Alert variant="success" withBorder>
      <Icon.Info variant="fill" />
      Hello world
    </Alert>
  </div>;

const AlertWarning: Story<{}> = (args) => {
  return (
    <div className='displayAsBlock'>
        
        <Alert variant="warning">Hello world</Alert>
        <Alert variant="warning">
          <Icon.Info variant="fill" />
          Hello world
        </Alert>
        <Alert variant="warning" withBorder>
          <Icon.Info variant="fill" />
          Hello world
        </Alert>
    </div>
  );
};

const AlertDanger: Story<{}> = (args) => {
  return (
    <div className='displayAsBlock'>
      <Alert variant="danger">Hello world</Alert>
      <Alert variant="danger">
        <Icon.Info variant="fill" />
        Hello world
      </Alert>
      <Alert variant="danger" withBorder>
        <Icon.Info variant="fill" />
        Hello world
      </Alert>
    </div>
  );
};

const AlertColor: Story<{}> = (args) => {
  return (
    <div className='displayAsBlock'>
      Specify color value for a custom background color for the alert: 
      &lt;Alert color='purple' &gt;
      <Alert color="purple">Hello world</Alert>
      <Alert color="purple">
        <Icon.Info variant="fill" />
        Hello world
      </Alert>
      <Alert color="purple" withBorder>
        <Icon.Info variant="fill" />
        Hello world
      </Alert>
    </div>
  );
};

export const Props = Template.bind({});
// Props.args = {
//   color: 'ColorName',
//   withBorder: false,
//   children: 'Any content',
//   variant: 'primary',
// };

export const Primary = PrimaryButton.bind({});
export const Success = AlertSuccess.bind({});
export const Warning = AlertWarning.bind({});
export const Danger = AlertDanger.bind({});
export const Colorvalue = AlertColor.bind({});
