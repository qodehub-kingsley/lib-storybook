import React from "react";
import PropTypes from "prop-types";

/**
 * props definition
 */
const propTypes = {
  width: PropTypes.number,
  color: PropTypes.string,
  height: PropTypes.number,
};

const defaultProps = {
  width: 24,
  height: 24,
  color: "var(--black-pearl)",
};

const Copy = ({ width, height, color, ...props }) => (
  <svg
    fill="none"
    width={width}
    height={height}
    viewBox="0 0 24 24"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M17 4H11C10.4477 4 10 4.44772 10 5V6H13C14.6569 6 16 7.34315 16 9V16H17C17.5523 16 18 15.5523 18 15V5C18 4.44772 17.5523 4 17 4ZM16 18V19C16 20.6569 14.6569 22 13 22H7C5.34315 22 4 20.6569 4 19V9C4 7.34315 5.34315 6 7 6H8V5C8 3.34315 9.34315 2 11 2H17C18.6569 2 20 3.34315 20 5V15C20 16.6569 18.6569 18 17 18H16ZM6 9C6 8.44772 6.44772 8 7 8H13C13.5523 8 14 8.44772 14 9V19C14 19.5523 13.5523 20 13 20H7C6.44772 20 6 19.5523 6 19V9Z"
      fill={color}
    />
  </svg>
);

Copy.propTypes = propTypes;
Copy.defaultProps = defaultProps;

export default Copy;
