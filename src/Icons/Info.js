import React from "react";
import PropTypes from "prop-types";

/**
 * props definition
 */
const propTypes = {
  variant: PropTypes.oneOf(["fill", "outline"]),
  height: PropTypes.number,
  color: PropTypes.string,
  width: PropTypes.number,
};

const defaultProps = {
  width: 24,
  height: 24,
  variant: null,
  color: "var(--black-pearl)",
};

const Info = ({ color, variant, width, height, ...props }) => {
  return (
    <React.Fragment>
      {variant === "fill" ? (
        <svg
          fill="none"
          viewBox="0 0 24 24"
          width={width}
          height={height}
          xmlns="http://www.w3.org/2000/svg"
          {...props}
        >
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22ZM12 5C12.5523 5 13 5.44772 13 6V14C13 14.5523 12.5523 15 12 15C11.4477 15 11 14.5523 11 14V6C11 5.44772 11.4477 5 12 5ZM13 18C13 18.5523 12.5523 19 12 19C11.4477 19 11 18.5523 11 18C11 17.4477 11.4477 17 12 17C12.5523 17 13 17.4477 13 18Z"
            fill={color}
          />
        </svg>
      ) : (
        <svg
          fill="none"
          viewBox="0 0 24 24"
          width={width}
          height={height}
          xmlns="http://www.w3.org/2000/svg"
          {...props}
        >
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M20 12C20 16.4183 16.4183 20 12 20C7.58172 20 4 16.4183 4 12C4 7.58172 7.58172 4 12 4C16.4183 4 20 7.58172 20 12ZM22 12C22 17.5228 17.5228 22 12 22C6.47715 22 2 17.5228 2 12C2 6.47715 6.47715 2 12 2C17.5228 2 22 6.47715 22 12ZM12 6C12.5523 6 13 6.44772 13 7V13C13 13.5523 12.5523 14 12 14C11.4477 14 11 13.5523 11 13V7C11 6.44772 11.4477 6 12 6ZM13 17C13 17.5523 12.5523 18 12 18C11.4477 18 11 17.5523 11 17C11 16.4477 11.4477 16 12 16C12.5523 16 13 16.4477 13 17Z"
            fill={color}
          />
        </svg>
      )}
    </React.Fragment>
  );
};

Info.propTypes = propTypes;
Info.defaultProps = defaultProps;

export default Info;
