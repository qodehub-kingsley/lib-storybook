import React from "react";
import PropTypes from "prop-types";

/**
 * props definition
 */
const propTypes = {
  width: PropTypes.number,
  color: PropTypes.string,
  height: PropTypes.number,
  variant: PropTypes.string,
};

const defaultProps = {
  width: 24,
  height: 24,
  variant: "default",
  color: "var(--black-pearl)",
};

const Search = ({ width, height, variant, color, ...props }) => (
  <svg
    fill="none"
    width={width}
    height={height}
    viewBox="0 0 24 24"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M15 9.5C15 12.5376 12.5376 15 9.5 15C6.46243 15 4 12.5376 4 9.5C4 6.46243 6.46243 4 9.5 4C12.5376 4 15 6.46243 15 9.5ZM14.0491 15.4633C12.7873 16.4274 11.2105 17 9.5 17C5.35786 17 2 13.6421 2 9.5C2 5.35786 5.35786 2 9.5 2C13.6421 2 17 5.35786 17 9.5C17 11.2105 16.4274 12.7873 15.4633 14.0491L20.7071 19.2929C21.0976 19.6834 21.0976 20.3166 20.7071 20.7071C20.3166 21.0976 19.6834 21.0976 19.2929 20.7071L14.0491 15.4633Z"
      fill={color}
    />
  </svg>
);

Search.propTypes = propTypes;
Search.defaultProps = defaultProps;

export default Search;
