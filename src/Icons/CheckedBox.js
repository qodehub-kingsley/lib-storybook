import React from "react";
import PropTypes from "prop-types";

/**
 * props definition
 */
const propTypes = {
  variant: PropTypes.oneOf(["round", "default"]),
  height: PropTypes.number,
  color: PropTypes.string,
  width: PropTypes.number,
};

const defaultProps = {
  width: 24,
  height: 24,
  variant: null,
  color: "var(--black-pearl)",
};

const CheckedBox = ({ color, variant, width, height, ...props }) => {
  return (
    <React.Fragment>
      <svg
        fill="none"
        viewBox="0 0 24 24"
        width={width}
        height={height}
        xmlns="http://www.w3.org/2000/svg"
        {...props}
      >
        {variant === "round" ? (
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22ZM17.7071 9.70711C18.0976 9.31658 18.0976 8.68342 17.7071 8.29289C17.3166 7.90237 16.6834 7.90237 16.2929 8.29289L10 14.5858L7.70711 12.2929C7.31658 11.9024 6.68342 11.9024 6.29289 12.2929C5.90237 12.6834 5.90237 13.3166 6.29289 13.7071L9.29289 16.7071C9.68342 17.0976 10.3166 17.0976 10.7071 16.7071L17.7071 9.70711Z"
            fill={color}
          />
        ) : (
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M7 3C4.79086 3 3 4.79086 3 7V17C3 19.2091 4.79086 21 7 21H17C19.2091 21 21 19.2091 21 17V7C21 4.79086 19.2091 3 17 3H7ZM17.7071 9.70711C18.0976 9.31658 18.0976 8.68342 17.7071 8.29289C17.3166 7.90237 16.6834 7.90237 16.2929 8.29289L10 14.5858L7.70711 12.2929C7.31658 11.9024 6.68342 11.9024 6.29289 12.2929C5.90237 12.6834 5.90237 13.3166 6.29289 13.7071L9.29289 16.7071C9.68342 17.0976 10.3166 17.0976 10.7071 16.7071L17.7071 9.70711Z"
            fill={color}
          />
        )}
      </svg>
    </React.Fragment>
  );
};

CheckedBox.propTypes = propTypes;
CheckedBox.defaultProps = defaultProps;

export default CheckedBox;
