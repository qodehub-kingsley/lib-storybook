import React from "react";
import PropTypes from "prop-types";

/**
 * props definition
 */
const propTypes = {
  variant: PropTypes.oneOf(["round", "default"]),
  height: PropTypes.number,
  color: PropTypes.string,
  width: PropTypes.number,
};

const defaultProps = {
  width: 24,
  height: 24,
  variant: null,
  color: "var(--black-pearl)",
};

const ChevronUp = ({ color, variant, width, height, ...props }) => {
  return (
    <React.Fragment>
      <svg
        fill="none"
        viewBox="0 0 24 24"
        width={width}
        height={height}
        xmlns="http://www.w3.org/2000/svg"
        {...props}
      >
        {variant === "round" ? (
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            fill={color}
            d="M20 12C20 16.4183 16.4183 20 12 20C7.58172 20 4 16.4183 4 12C4 7.58172 7.58172 4 12 4C16.4183 4 20 7.58172 20 12ZM22 12C22 17.5228 17.5228 22 12 22C6.47715 22 2 17.5228 2 12C2 6.47715 6.47715 2 12 2C17.5228 2 22 6.47715 22 12ZM15.2929 13.7071C15.6834 14.0976 16.3166 14.0976 16.7071 13.7071C17.0976 13.3166 17.0976 12.6834 16.7071 12.2929L12.7071 8.29289C12.3166 7.90237 11.6834 7.90237 11.2929 8.29289L7.29289 12.2929C6.90237 12.6834 6.90237 13.3166 7.29289 13.7071C7.68342 14.0976 8.31658 14.0976 8.70711 13.7071L12 10.4142L15.2929 13.7071Z"
          />
        ) : (
          <path
            strokeWidth="2"
            d="M16 13L12 9L8 13"
            strokeLinecap="round"
            strokeLinejoin="round"
            stroke={color}
          />
        )}
      </svg>
    </React.Fragment>
  );
};

ChevronUp.propTypes = propTypes;
ChevronUp.defaultProps = defaultProps;

export default ChevronUp;
