import React from "react";
import PropTypes from "prop-types";

/**
 * props definition
 */
const propTypes = {
  width: PropTypes.number,
  color: PropTypes.string,
  height: PropTypes.number,
};

const defaultProps = {
  width: 24,
  height: 24,
  color: "var(--black-pearl)",
};

const Bank = ({ width, height, color, ...props }) => (
  <svg
    fill="none"
    width={width}
    height={height}
    viewBox="0 0 24 24"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M20 7V5.63961L12 4.03961L4 5.63961V7H5H7H11H13H17H19H20ZM13 9H17V16H13V9ZM11 16V9H7V16H11ZM5 16V9H4C2.89543 9 2 8.10457 2 7V5.63961C2 4.68625 2.67292 3.86542 3.60777 3.67845L11.6078 2.07845C11.8667 2.02667 12.1333 2.02667 12.3922 2.07845L20.3922 3.67845C21.3271 3.86542 22 4.68625 22 5.63961V7C22 8.10457 21.1046 9 20 9H19V16H21C21.5523 16 22 16.4477 22 17V21C22 21.5523 21.5523 22 21 22H3C2.44772 22 2 21.5523 2 21V17C2 16.4477 2.44772 16 3 16H5ZM4 20V18H20V20H4Z"
      fill={color}
    />
  </svg>
);

Bank.propTypes = propTypes;
Bank.defaultProps = defaultProps;

export default Bank;
