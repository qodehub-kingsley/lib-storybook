import React from "react";
import PropTypes from "prop-types";

/**
 * props definition
 */
const propTypes = {
  width: PropTypes.number,
  color: PropTypes.string,
  height: PropTypes.number,
};

const defaultProps = {
  width: 24,
  height: 24,
  color: "var(--black-pearl)",
};

const Upload = ({ width, height, color, ...props }) => (
  <svg
    fill="none"
    width={width}
    height={height}
    viewBox="0 0 24 24"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M18 19C18 19.5523 17.5523 20 17 20H7C6.44772 20 6 19.5523 6 19V5C6 4.44772 6.44771 4 7 4H13.7574C14.0226 4 14.2769 4.10536 14.4645 4.29289L16.5858 6.41421L17.7071 7.53553C17.8946 7.72307 18 7.97742 18 8.24264V19ZM4 5C4 3.34315 5.34315 2 7 2H13.7574C14.553 2 15.3161 2.31607 15.8787 2.87868L18 5L19.1213 6.12132C19.6839 6.68393 20 7.44699 20 8.24264V19C20 20.6569 18.6569 22 17 22H7C5.34315 22 4 20.6569 4 19V5ZM12.7071 7.29289C12.3166 6.90237 11.6834 6.90237 11.2929 7.29289L8.29289 10.2929C7.90237 10.6834 7.90237 11.3166 8.29289 11.7071C8.68342 12.0976 9.31658 12.0976 9.70711 11.7071L11 10.4142V16C11 16.5523 11.4477 17 12 17C12.5523 17 13 16.5523 13 16V10.4142L14.2929 11.7071C14.6834 12.0976 15.3166 12.0976 15.7071 11.7071C16.0976 11.3166 16.0976 10.6834 15.7071 10.2929L12.7071 7.29289Z"
      fill={color}
    />
  </svg>
);

Upload.propTypes = propTypes;
Upload.defaultProps = defaultProps;

export default Upload;
