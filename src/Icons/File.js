import React from "react";
import PropTypes from "prop-types";

/**
 * props definition
 */
const propTypes = {
  width: PropTypes.number,
  color: PropTypes.string,
  height: PropTypes.number,
};

const defaultProps = {
  width: 24,
  height: 24,
  color: "var(--black-pearl)",
};

const File = ({ width, height, color, ...props }) => (
  <svg
    fill="none"
    width={width}
    height={height}
    viewBox="0 0 24 24"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M5 5C5 3.89543 5.89543 3 7 3H13.7574C14.2878 3 14.7965 3.21071 15.1716 3.58579L17.2929 5.70711L18.4142 6.82843C18.7893 7.2035 19 7.71221 19 8.24264V19C19 20.1046 18.1046 21 17 21H7C5.89543 21 5 20.1046 5 19V5Z"
      stroke={color}
      strokeWidth="2"
    />
  </svg>
);

File.propTypes = propTypes;
File.defaultProps = defaultProps;

export default File;
