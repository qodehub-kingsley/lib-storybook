import React from "react";
import PropTypes from "prop-types";

/**
 * props definition
 */
const propTypes = {
  width: PropTypes.number,
  color: PropTypes.string,
  height: PropTypes.number,
};

const defaultProps = {
  width: 24,
  height: 24,
  color: "var(--black-pearl)",
};

const Profile = ({ width, height, color, ...props }) => (
  <svg
    fill="none"
    width={width}
    height={height}
    viewBox="0 0 24 24"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M12 11C14.2091 11 16 9.20914 16 7C16 4.79086 14.2091 3 12 3C9.79085 3 7.99999 4.79086 7.99999 7C7.99999 9.20914 9.79085 11 12 11ZM19.9377 20.0026C20.0065 20.5505 19.5524 21 19.0001 21H12.0001L5.00007 21C4.44779 21 3.99362 20.5505 4.06247 20.0026C4.28283 18.2486 5.07998 16.6064 6.34322 15.3431C7.84351 13.8429 9.87834 13 12.0001 13C14.1218 13 16.1566 13.8429 17.6569 15.3431C18.9202 16.6064 19.7173 18.2486 19.9377 20.0026Z"
      fill={color}
    />
  </svg>
);

Profile.propTypes = propTypes;
Profile.defaultProps = defaultProps;

export default Profile;
