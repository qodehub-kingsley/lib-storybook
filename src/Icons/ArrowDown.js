import React from "react";
import PropTypes from "prop-types";

/**
 * props definition
 */
const propTypes = {
  width: PropTypes.number,
  color: PropTypes.string,
  height: PropTypes.number,
};

const defaultProps = {
  width: 24,
  height: 24,
  color: "var(--black-pearl)",
};

const ArrowDown = ({ width, height, color, ...props }) => (
  <svg
    fill="none"
    width={width}
    height={height}
    viewBox="0 0 24 24"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      stroke={color}
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
      d="M5 14L12 21M12 21L19 14M12 21L12 3"
    />
  </svg>
);

ArrowDown.propTypes = propTypes;
ArrowDown.defaultProps = defaultProps;

export default ArrowDown;
