import { useEffect } from "react";
import PropTypes from "prop-types";

/**
 * props definition
 */
const propTypes = {
  title: PropTypes.string.isRequired,
};

const defaultProps = {
  title: "",
};

const useTitle = (title) => {
  useEffect(() => {
    document.title = title.trim() + " | " + process.env.REACT_APP_TITLE_SUFFIX;
  }, [title]);
};

useTitle.propTypes = propTypes;
useTitle.defaultProps = defaultProps;

export default useTitle;
