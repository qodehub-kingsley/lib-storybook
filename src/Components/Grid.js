import styled from "styled-components";
import PropTypes from "prop-types";

const propTypes = {
  gap: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  sm: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  md: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  lg: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  xl: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  flow: PropTypes.string,
};

const defaultProps = {
  gap: "32px",
  flow: "column",
};

const Grid = styled.div`
  width: 100%;
  display: grid;
  grid-gap: ${({ gap }) => gap};
  grid-template-columns: ${({ sm }) => sm || "auto"};

  @media (min-width: 768px) {
    grid-template-columns: ${({ sm, md }) => md || sm || "auto"};
  }

  @media (min-width: 992px) {
    grid-template-columns: ${({ sm, md, lg }) => lg || md || sm || "auto"};
  }

  @media (min-width: 1200px) {
    grid-template-columns: ${({ sm, md, lg, xl }) =>
      xl || lg || md || sm || "auto"};
  }
`;

Grid.propTypes = propTypes;
Grid.defaultProps = defaultProps;

export default Grid;
