import React, { Fragment, useRef } from "react";
import { toast } from "react-toastify";
import PropTypes from "prop-types";
import styled from "styled-components";
import Form from "react-bootstrap/Form";

import UploadIcon from "../Icons/Upload";
import FileIcon from "../Icons/File";
import useWidth from "../Hooks/useWidth";
import Popover from "./Popover";
import Notify from "../Components/Notify";
import Close from "../Icons/Close";

/**
 * prop definitions
 */
const propTypes = {
  /**
   * function to set value
   */
  setFieldValue: PropTypes.func,
  /**
   * input accept
   */
  accept: PropTypes.string,
  /**
   * label for uploader
   */
  label: PropTypes.string.isRequired,
  /**
   * option to cancel upload
   */
  withClear: PropTypes.bool,
  /**
   * max file upload size (mb)
   */
  max: PropTypes.number,
  /**
   * file to upload
   */
  value: PropTypes.any.isRequired,
  /**
   * tip for information
   */
  tip: PropTypes.string,
};

const defaultProps = {
  withClear: false,
  max: 2,
};

/**
 *
 * @param {*} setFieldValue function to set value
 * @param {*} name upload name
 * @param {*} value file to upload
 * @param {*} max max upload size
 * @param {*} label label for uploader
 * @param {*} accept input accept
 * @param {*} tip tip for information
 */
const Upload = ({
  setFieldValue,
  withClear,
  accept,
  label,
  value,
  max,
  tip,
  ...props
}) => {
  /**
   * ref
   */
  let img = useRef();

  const width = useWidth();

  /**
   * function
   */
  const handleUpload = (file) => {
    if (file) {
      const size = file?.size;

      if (size > max * 1000000) {
        toast(
          <Notify body={`File size must be less than ${max}mb`} type="error" />,
        );
      } else {
        setFieldValue(file);
      }
    } else {
      img.value = "";
      setFieldValue("");
    }
  };

  return (
    <Wrapper>
      {value ? (
        <Form.Group className="mb-0 active-wrapper">
          {label && (
            <Form.Label>
              <span>{label}</span>
            </Form.Label>
          )}
          <ActiveWrapper {...{ ...props, withClear }}>
            <div className="content mr-auto">
              <div className="content__body text-truncate">
                <FileIcon className="mr-3" color="var(--chateau-green)" />
                <p className="text-truncate text-body black-pearl-text">
                  {value?.name}
                </p>
              </div>
            </div>
            {withClear && (
              <Close
                color="var(--gull-grey)"
                className="ml-3 cursor-pointer"
                onClick={() => handleUpload(null)}
              />
            )}
          </ActiveWrapper>
        </Form.Group>
      ) : (
        <Form.Group className="mb-0 inactive-wrapper">
          {label && (
            <Form.Label className="d-flex justify-content-between">
              <span>{label}</span>
              {tip && width < 767 && <Popover placement="top">{tip}</Popover>}
            </Form.Label>
          )}
          <div className="d-flex align-items-center">
            <InactiveWrapper onClick={() => img.click()} {...props}>
              <UploadIcon className="mr-3" color="var(--chateau-green)" />
              <span className="text-sub">Browse file to upload</span>
            </InactiveWrapper>

            {tip && width >= 768 && (
              <span className="ml-2">
                <Popover placement="top">{tip}</Popover>
              </span>
            )}
          </div>
        </Form.Group>
      )}

      <input
        type="file"
        accept={accept}
        className="d-none"
        ref={(ref) => (img = ref)}
        onChange={({ currentTarget: { files } }) => handleUpload(files[0])}
      />
    </Wrapper>
  );
};

/**
 * styles
 */
const Wrapper = styled.div`
  .inactive-wrapper {
    max-width: 404px;
  }

  .active-wrapper {
    max-width: 364px;
    // Fix text and uploaded file overlapping
    display: flex;
    flex-direction: column;
    row-gap: 16px;
  }
`;

const ActiveWrapper = styled.div`
  display: ${({ withClear }) => (withClear ? "flex" : "inline-flex")};
  justify-content: space-between;
  align-items: center;
  margin-right: auto;
  height: 64px;

  .content {
    background-color: var(--desert-storm);
    border-radius: 8px;
    max-width: 332px;
    padding: 0.5rem;
    width: 100%;

    &__body {
      background-color: #fff;
      align-items: center;
      border-radius: 8px;
      padding: 12px 16px;
      display: flex;
    }
  }
`;

const InactiveWrapper = styled.div`
  border: 1px dashed var(--gull-grey);
  background: var(--snow-drift);
  color: var(--chateau-green);
  justify-content: center;
  box-sizing: border-box;
  align-items: center;
  border-radius: 8px;
  max-width: 364px;
  cursor: pointer;
  display: flex;
  height: 72px;
  width: 100%;
`;

Upload.propTypes = propTypes;
Upload.defaultProps = defaultProps;

export default Upload;
