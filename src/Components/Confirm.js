import React from "react";
import { createConfirmation, confirmable } from "react-confirm";
import PropTypes from "prop-types";
import Modal from "react-bootstrap/Modal";

import Button from "./Button";

/**
 * props definition
 */
const propTypes = {
  show: PropTypes.bool,
  header: PropTypes.string,
  msg: PropTypes.any.isRequired,
  proceed: PropTypes.func.isRequired,
};

const Dialog = ({ show, proceed, msg, header, options }) => {
  return (
    <Modal onHide={() => proceed(false)} show={show} centered>
      <Modal.Body className="p-4">
        {header && <h3 className="mb-4">{header}</h3>}
        <p className="mb-4 text-body">{msg}</p>
        <div className="d-flex align-items-center justify-content-end mx-n2">
          <div className="px-2">
            <Button onClick={() => proceed(false)} outline children="Cancel" />
          </div>
          <div className="px-2">
            <Button onClick={() => proceed(true)} children="Confirm" />
          </div>
        </div>
      </Modal.Body>
    </Modal>
  );
};

const Confirm = createConfirmation(confirmable(Dialog));

Confirm.propTypes = propTypes;

export default Confirm;
