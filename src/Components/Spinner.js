import React from "react";
import PropTypes from "prop-types";
import ClipLoader from "react-spinners/ClipLoader";

/**
 * props definition
 */
const propTypes = {
  size: PropTypes.number,
  color: PropTypes.string,
};

const defaultProps = {
  size: 50,
  color: "#555",
};

/**
 *
 * @param size size of spinner
 * @param color color of spinner
 */
const Spinner = ({ size, color, ...props }) => {
  return (
    <div className="d-flex align-items-center justify-content-center mx-auto">
      <ClipLoader size={size} color={color} {...props} />
    </div>
  );
};

Spinner.propTypes = propTypes;
Spinner.defaultProps = defaultProps;

export default Spinner;
