import React from "react";
import flags from "emoji-flags";
import countryList from "react-select-country-list";

import Select from "./Select";

const Countries = (props) => {
  /**
   * variable
   */
  const options = ((options) => {
    return options.map(({ label, value }) => ({
      value,
      label: `${flags.countryCode(value)?.emoji} ${label}`,
    }));
  })(countryList().getData());

  return <Select {...props} options={options} placholder="" />;
};

export default Countries;
