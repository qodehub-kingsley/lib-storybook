import React, { Fragment } from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import ReactSelect, { components } from "react-select";

import ErrorBoundary from "../Utils/ErrorBoundary";
import ChevronDown from "../Icons/ChevronDown";
import ChevronUp from "../Icons/ChevronUp";

/**
 * components
 */
const DropdownIndicator = ({ children, isFocused, hasValue, ...props }) => {
  const Icon = isFocused && !hasValue ? ChevronUp : ChevronDown;
  return (
    <components.DropdownIndicator {...props}>
      <Icon />
    </components.DropdownIndicator>
  );
};

const Content = ({ data, ...props }) => (
  <div className="d-flex align-items-center cursor-pointer">
    {data?.avatar && (
      <Fragment>
        {data?.isUrl && <Avatar avatar={data.avatar} />}
        {data?.isSvg && <data.avatar width={24} height={24} className="mr-2" />}
      </Fragment>
    )}
    <p className="mb-0">{data?.label}</p>
  </div>
);

/**
 * styles
 */
const styles = {
  control: (styles, { isFocused, isDisabled }) => ({
    ...styles,
    borderColor: "transparent !important",
    backgroundColor: isDisabled ? "var(--desert-storm)" : "transparent",
    borderRadius: "8px",
    boxShadow: isFocused ? "none" : "none",
    height: "100%",
  }),
  menuList: () => ({
    paddingTop: 0,
    paddingBottom: 0,
    overflow: "auto",
    maxHeight: 200,
  }),
  menuPortal: (styles) => ({ ...styles, zIndex: 999 }),
  menu: (styles) => ({
    ...styles,
    boxShadow: "2px 2px 8px rgba(0, 0, 0, 0.12)",
    border: "none !important",
    backgroundColor: "#fff",
    paddingBottom: "4px",
    paddingTop: "4px",
    zIndex: 999,
  }),
  option: (styles, { isSelected }) => ({
    ...styles,
    backgroundColor:
      (isSelected && "var(--desert-storm)!important") || "#fff!important",
    color: "var(--black-pearl)",
    padding: "10px 16px",
    lineHeight: "22px",
    fontSize: "14px",
    outline: "none",
  }),
  multiValue: (styles) => ({ ...styles, borderRadius: 8 }),
  multiValueLabel: (styles) => ({ ...styles, paddingLeft: 8, paddingRight: 8 }),
  valueContainer: (styles) => ({
    ...styles,
    height: "100%",
    padding: "0px 16px",
  }),
  singleValue: (styles) => ({ ...styles, color: "var(--subtle-grey)" }),
  indicatorsContainer: (styles) => ({
    ...styles,
    paddingRight: 4,
  }),
  placeholder: (styles) => ({
    ...styles,
    color: "var(--gull-grey)",
  }),
};

/**
 * styled component styles
 */
const Avatar = styled.div`
  background-image: ${({ avatar }) => `url(${avatar})`};
  background-color: var(--athens-grey);
  background-repeat: no-repeat;
  background-position: center;
  background-size: cover;
  border-radius: 50%;
  margin-right: 8px;
  height: 24px;
  width: 24px;
`;

const SelectWrapper = styled(ReactSelect)`
  .Select-menu-outer {
    position: relative;
    z-index: 999;
  }
`;

/**
 * props definition
 */
const propTypes = {
  onlyOptions: PropTypes.arrayOf(PropTypes.object),
  options: PropTypes.arrayOf(PropTypes.object),
  value: PropTypes.string.isRequired,
};

const defaultProps = {
  onlyOptions: [],
  options: [],
  value: "",
};

/**
 *
 * @param options array of objects for selecetion
 * @param onlyOptions only options to display to user
 * @param value select option to display
 */
const Select = ({ options, onlyOptions, value, ...props }) => {
  const SelectComponents = {
    MultiValueRemove: () => null,
    IndicatorSeparator: () => null,
    DropdownIndicator,
    Option: (props) => (
      <components.Option {...props}>
        <Content {...props} />
      </components.Option>
    ),
    SingleValue: (props) => (
      <components.SingleValue {...props}>
        <Content {...props} />
      </components.SingleValue>
    ),
  };

  let _options = options;
  let _value = "";

  if (onlyOptions && onlyOptions.length > 0) {
    _options = options.filter(
      (option) =>
        !!onlyOptions.find(
          (_option) => _option?.toLowerCase() === option.value?.toLowerCase(),
        ),
    );
  }

  if (value) {
    if (!Array.isArray(value)) {
      _value =
        options.find(
          (option) =>
            (option.value || "").toString()?.toLowerCase() ===
              (value || "").toString()?.toLowerCase() || "",
        ) || "";
    }

    if (Array.isArray(value)) {
      _value =
        options.filter((option) =>
          value
            ?.map((v) => String(v))
            .includes((option.value || "").toString()),
        ) || [];
    }
  }

  return (
    <ErrorBoundary>
      <SelectWrapper
        {...props}
        value={_value}
        styles={styles}
        options={_options}
        components={SelectComponents}
      />
    </ErrorBoundary>
  );
};

Select.propTypes = propTypes;
Select.defaultProps = defaultProps;

export default Select;
