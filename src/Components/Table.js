import React from "react";
import Tbl from "react-bootstrap/Table";
import PropTypes from "prop-types";

/**
 * props definition
 */
const childProps = {
  center: PropTypes.bool,
  right: PropTypes.bool,
  left: PropTypes.bool,
  className: PropTypes.string,
  children: PropTypes.any,
};

const Td = ({ center, right, left, children, className, ...props }) => {
  return (
    <td {...props}>
      <div
        className={`td justify-content-${
          (center && "center") || (right && "end") || (left && "start")
        } ${className} `}
      >
        {children}
      </div>
    </td>
  );
};

const Th = ({ children, left, right, center, className, ...props }) => {
  return (
    <th {...props}>
      <div
        className={`th justify-content-${
          (center && "center") || (right && "end") || (left && "start")
        }`}
      >
        {children}
      </div>
    </th>
  );
};

const Table = ({ children, ...props }) => {
  return (
    <div className="table-responsive h-100">
      <Tbl {...props}>{children}</Tbl>
    </div>
  );
};

Th.propTypes = childProps;
Td.propTypes = childProps;

export { Td, Th };
export default Table;
