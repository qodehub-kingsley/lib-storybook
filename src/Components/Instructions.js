import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";

import Check from "../Icons/CheckedBox";

/**
 * props definition
 */
const propTypes = {
  /**
   * Array of items to be displayed
   */
  instructions: PropTypes.arrayOf(PropTypes.any).isRequired,
};

const Instructions = ({ instructions, ...props }) => {
  return (
    <Wrapper {...props}>
      {instructions.map((instruction, index) => (
        <Item key={index}>
          <Check variant="round" color="var(--gull-grey)" />
          <p className="my-0 text-body">{instruction}</p>
        </Item>
      ))}
    </Wrapper>
  );
};

/**
 * styles
 */
const Wrapper = styled.div`
  display: grid;
  gap: 24px;
`;

const Item = styled.div`
  display: flex;

  svg {
    margin-right: 24px;
    flex: 0 0 1.5rem;
  }

  p {
    color: var(--subtle-grey);
  }
`;

Instructions.propTypes = propTypes;

export default Instructions;
