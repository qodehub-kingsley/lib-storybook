import React from "react";
import PropTypes from "prop-types";

const propTypes = {
  header: PropTypes.string.isRequired,
  children: PropTypes.string.isRequired,
};

/**
 *
 * @param {*} header header of the data item
 * @param {*} children content of the data item
 */
const DataItem = ({ header, children, ...props }) => {
  return (
    <div {...props}>
      <p className="text-sub mb-1">{header}</p>
      <p className="mb-0 text-body black-pearl-text">{children}</p>
    </div>
  );
};

DataItem.propTypes = propTypes;

export default DataItem;
