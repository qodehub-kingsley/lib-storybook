import React, { Children, cloneElement } from "react";
import styled from "styled-components";
import PropTypes from "prop-types";

/**
 * props definition
 */
const propTypes = {
/**
 * specify color name to be used as a background
 */
  color: PropTypes.string,
/**
 * show border
 */
withBorder: PropTypes.bool,
/**
 * content of alert
 */
children: PropTypes.any.isRequired,
/**
 * variant of alert
 */
  variant: PropTypes.oneOf(["primary", "success", "warning", "danger"]),
};

const defaultProps = {
  withBorder: false,
  variant: 'primary',
};

/**
 *
 * @param {*} variant variant of alert
 * @param {*} withBorder show border
 * @param {*} color color of alert
 * @param {*} children content of alert
 */
const Alert = ({ variant, withBorder, color, children, ...props }) => {
  /**
   * variables
   */
  color = ((variant, color) => {
    if (color) {
      return color;
    }

    switch (variant) {
      case "primary":
        return "var(--radiant-blue)";
      case "success":
        return "var(--chateau-green)";
      case "warning":
        return "var(--casablanca)";
      case "danger":
        return "var(--cinnabar)";
      default:
        break;
    }
  })(variant, color);

  // check whether children has icon
  const content = ((children, color) => {
    const content = Children.toArray(children);

    if (content.length === 2) {
      const icon = cloneElement(content[0], { color }, null);
      return { icon: icon, children: content[1] };
    } else {
      return { children: content[0] };
    }
  })(children, color);

  return (
    <Wrapper
      {...{
        color,
        withBorder,
        icon: content?.icon,
        ...props,
      }}
    >
      {content?.icon && content.icon}
      <p className="my-0 text-sub">{content.children}</p>
    </Wrapper>
  );
};

/**
 * styles
 */
const Wrapper = styled.div`
  grid-template-columns: ${({ icon }) => (icon ? "24px auto" : "100%")};
  position: relative;
  border-radius: 8px;
  padding: 12px 16px;
  overflow: hidden;
  display: grid;
  z-index: 1;
  gap: 12px;

  border: ${({ withBorder, color }) =>
    withBorder ? `solid 1px ${color}` : "none"};

  &::before {
    z-index: -1;
    width: 100%;
    height: 100%;
    content: " ";
    opacity: 0.1;
    display: block;
    position: absolute;
    background-color: ${({ color }) => color};
  }

  p {
    color: var(--subtle-grey);
  }
`;

Alert.propTypes = propTypes;
Alert.defaultProps = defaultProps;

export default Alert;
