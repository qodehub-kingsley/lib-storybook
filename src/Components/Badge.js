import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";

/**
 * prop definition
 */
const propTypes = {
  /**
   * for badge background
   */
  bg: PropTypes.string,
  /**
   * text color
   */
  color: PropTypes.string,
  /**
   * data in badge
   */
  children: PropTypes.string.isRequired,
  /**
   * badge variant background and foreground color
   */
  variant: PropTypes.oneOf(["failed", "success", "processing"]),
};

const defaultProps = {
  color: "#fff",
};

/**
 *
 * @param {*} bg for badge background
 * @param {*} color text color
 * @param {*} children data in badge
 * @param {*} variant badge variant background and foreground color
 */
const Badge = ({ bg, color, children, variant, ...props }) => {
  /**
   * variables
   */
  bg = ((bg, variant) => {
    if (bg) {
      return bg;
    }

    switch (variant) {
      case "failed":
        return "var(--cinnabar)";
      case "success":
        return "var(--chateau-green)";
      case "processing":
        return "var(--radiant-blue)";
      default:
        break;
    }
  })(bg, variant);

  return (
    <Wrapper {...{ ...props, bg, color }}>
      <span className="text-sub -medium">{children}</span>
    </Wrapper>
  );
};

/**
 * styles
 */
const Wrapper = styled.div`
  border-radius: 28px;
  padding: 0.25rem 0.5rem;
  background-color: ${({ bg }) => bg};

  span {
    color: ${({ color }) => color};
  }
`;

Badge.propTypes = propTypes;
Badge.defaultProps = defaultProps;

export default Badge;
