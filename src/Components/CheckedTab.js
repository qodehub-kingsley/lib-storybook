import React, { Fragment } from "react";
import { CircularProgressbar, buildStyles } from "react-circular-progressbar";
import styled from "styled-components";
import PropTypes from "prop-types";

import UncheckedBox from "../Icons/UncheckedBox";
import CheckedBox from "../Icons/CheckedBox";
import useWidth from "../Hooks/useWidth";

import "react-circular-progressbar/dist/styles.css";

/**
 * props definition
 */
const propTypes = {
  onClick: PropTypes.func,
  activeKey: PropTypes.number,
  variant: PropTypes.oneOf(["sm", "lg", "icon"]),
  tabs: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string.isRequired,
      desc: PropTypes.string,
      icon: PropTypes.any,
    }),
  ).isRequired,
};

const defaultProps = {
  tabs: [],
  activeKey: 0,
  variant: "sm",
  onClick: () => {},
};

/**
 *
 * @param tabs tab data
 * @param variant type of tab design
 * @param activeKey active tab key
 */

const CheckedTab = ({ tabs, variant, activeKey, onClick, ...props }) => {
  /**
   * variables
   */
  const percentage = (((activeKey + 1) / tabs.length) * 100).toFixed(2);
  const width = useWidth();

  return (
    <Fragment>
      {variant === "sm" && (
        <SmWrapper width={width} {...props}>
          {width > 767 ? (
            <Fragment>
              {tabs?.map(({ title }, key) => (
                <SmItem
                  key={key}
                  current={activeKey === key}
                  onClick={() => onClick(key)}
                >
                  <span className="mr-lg-6">
                    {activeKey === key && (
                      <UncheckedBox variant="round" color="#fff" />
                    )}
                    {activeKey > key && (
                      <CheckedBox
                        variant="round"
                        color="var(--chateau-green)"
                      />
                    )}
                    {activeKey < key && (
                      <UncheckedBox variant="round" color="var(--gull-grey)" />
                    )}
                  </span>
                  <Header current={activeKey === key}>{title}</Header>
                </SmItem>
              ))}
            </Fragment>
          ) : (
            <Fragment>
              <Circle>
                <CircularProgressbar
                  circleRatio
                  strokeWidth={8}
                  value={percentage}
                  text={`${activeKey + 1}/${tabs.length}`}
                  styles={buildStyles({
                    pathColor: "var(--subtle-grey)",
                    trailColor: "var(--athens-grey)",
                    textColor: "var(--subtle-grey)",
                    textLineHeight: "22px",
                    strokeLinecap: "butt",
                    textSize: "24px",
                  })}
                />
              </Circle>
              <div>
                <p className="text-body -medium mb-0">
                  {tabs[activeKey].title}
                </p>
                {tabs[activeKey + 1] && (
                  <p className="text-sub gull-grey-text mb-0 mt-1">
                    Next: {tabs[activeKey + 1].title}
                  </p>
                )}
              </div>
            </Fragment>
          )}
        </SmWrapper>
      )}
      {variant === "lg" && (
        <LgWrapper {...props}>
          {tabs.map(({ title, desc }, key) => (
            <LgItem
              key={key}
              active={activeKey === key}
              onClick={() => onClick(key)}
            >
              <div>
                <p className="text-body -medium mb-0">{title}</p>
                <p className="text-sub mb-0 mt-1">{desc}</p>
              </div>
              {activeKey === key && <CheckedBox color="#fff" variant="round" />}
            </LgItem>
          ))}
        </LgWrapper>
      )}
      {variant === "icon" && (
        <IconWrapper {...props}>
          {tabs.map((tab, key) => (
            <IconItem
              key={key}
              active={activeKey === key}
              onClick={() => onClick(key)}
            >
              <tab.icon />
              <p className="text-body -medium mb-0 mt-2">{tab?.title}</p>
            </IconItem>
          ))}
        </IconWrapper>
      )}
    </Fragment>
  );
};

/**
 * styles
 */
const Header = styled.p`
  font-size: 16px;
  font-weight: 500;
  line-height: 24px;
  margin-bottom: 0px;
  color: ${({ current }) => (current ? "#fff" : "var(--gull-grey)")};
`;

const SmItem = styled.div`
  padding: 12px 24px;
  border-radius: 8px;
  text-align: center;
  background-color: ${({ current }) =>
    current ? "var(--chateau-green)" : "transparent"};

  @media (min-width: 768px) {
    svg {
      margin-bottom: 6px;
    }
  }

  @media (min-width: 992px) {
    display: flex;
    padding: 20px 24px;
    align-items: center;

    svg {
      margin-bottom: 0px;
    }
  }
`;
const Circle = styled.div`
  width: 64px;
  height: 64px;
`;
const SmWrapper = styled.div`
  display: grid;
  width: 100%;
  gap: 8px;

  @media (max-width: 757px) {
    background-color: var(--desert-storm);
    grid-template-columns: 64px auto;
    align-items: center;
    border-radius: 8px;
    padding: 16px;
    gap: 16px;
  }
`;

const LgItem = styled.div`
  border: solid 1px var(--athens-grey);
  border-radius: 8px;
  cursor: pointer;
  padding: 24px;

  grid-template-columns: auto 24px;
  justify-content: space-between;
  align-items: center;
  display: grid;
  gap: 24px;

  ${({ active }) =>
    active &&
    `
    background-color: var(--chateau-green);
    color: #fff;
  `}
`;
const LgWrapper = styled.div`
  display: grid;
  gap: 16px;
`;

const IconWrapper = styled.div`
  display: grid;
  gap: 16px;

  @media (min-width: 768px) {
    gap: 32px;
  }
`;

const IconItem = styled.div`
  padding: 1.5rem;
  cursor: pointer;
  text-align: center;
  border-radius: 8px;
  transition: ease all 0.25s;
  background-color: ${({ active }) =>
    active ? "var(--chateau-green)" : "#fff"};
  border: solid 1px
    ${({ active }) => (active ? "var(--chateau-green)" : "var(--athens-grey)")};
  color: ${({ active }) => (active ? "#fff" : "var(--black-pearl)")};

  svg {
    path {
      fill: ${({ active }) => (active ? "#fff" : "var(--black-pearl)")};
    }
  }
`;

/**
 * props assignment
 */
CheckedTab.propTypes = propTypes;
CheckedTab.defaultProps = defaultProps;

export default CheckedTab;
