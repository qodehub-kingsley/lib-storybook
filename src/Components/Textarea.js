import React from "react";
import Area from "react-textarea-autosize";

/**
 *
 * @param {*} field from formik
 */
const Textarea = ({ field, form: { touched, errors }, ...props }) => {
  return <Area {...field} {...props} />;
};

export default Textarea;
