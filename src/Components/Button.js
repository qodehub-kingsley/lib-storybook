import React from "react";
import PropTypes from "prop-types";
import Btn from "react-bootstrap/Button";

import Spinner from "./Spinner";

/**
 * props definition
 */
const propTypes = {
  /**
   * submitting button state
   */
  isSubmitting: PropTypes.bool,
  /**
   * loading color when button is loading
   */
  loadingColor: PropTypes.string,
  /**
   * content of button
   */
  children: PropTypes.any.isRequired,
  /**
   * disabled state
   */
  isValid: PropTypes.bool,
  /**
   * type of button
   */
  outline: PropTypes.bool,
  /**
   * type of button
   */
  round: PropTypes.bool,
};

const defaultProps = {
  isSubmitting: false,
  loadingColor: "#fff",
  outline: false,
  isValid: true,
  round: false,
};

/**
 *
 * @param isSubmitting submitting button state
 * @param isValid disabled state
 * @param loadingColor loading color when button is loading
 * @param children content of button
 */

const Button = ({
  isSubmitting,
  loadingColor,
  children,
  isValid,
  outline,
  round,
  ...props
}) => {
  const type = (({ outline, round }) => {
    let className;

    if (outline) {
      className = `${className} btn--outline`;
    }
    if (round) {
      className = `${className} btn--round`;
    }

    return className || "btn--default";
  })({ outline, round });

  return (
    <Btn
      {...props}
      variant="default"
      className={`text-truncate ${type} ${props.className}`}
      {...(isSubmitting && { disabled: true })}
      {...(!isValid && { disabled: true })}
    >
      {isSubmitting ? <Spinner size={25} color={loadingColor} /> : children}
    </Btn>
  );
};

Button.propTypes = propTypes;
Button.defaultProps = defaultProps;

export default Button;
