import React, { Fragment } from "react";
import PropTypes from "prop-types";

import Check from "../Icons/Check";
import Close from "../Icons/Close";

/**
 * props definition
 */
const propTypes = {
  /**
   * Title to display in the notifyer, defaults to null
   */
  title: PropTypes.string,
  /**
   * Text to display in the notify
   */
  body: PropTypes.string.isRequired,
  type: PropTypes.string,
};

const defaultProps = {
  body: "",
  title: null,
  type: "default",
};

const Notify = ({ title=null, body, type }) => (
  <Fragment>
    {title && (
      <div className="heading d-flex justify-content-between align-items-center">
        <h3 className="subtle-grey-text">{title && title}</h3>
        <Close className="ml-auto" width={16} height={16} />
      </div>
    )}
    <div className="content-container d-flex">
      {type && (
        <span className="icon">
          {type === "success" && (
            <Check color="var(--chateau-green)" width={32} height={32} />
          )}
          {type === "error" && (
            <Close
              variant="round"
              color="var(--cinnabar)"
              width={32}
              height={32}
            />
          )}
        </span>
      )}
      <p className="text-body subtle-grey-text mb-0 mr-2">{body}</p>
      {!title && <Close className="ml-auto" width={16} height={16} />}
    </div>
  </Fragment>
);

Notify.propTypes = propTypes;
Notify.defaultProps = defaultProps;

export default Notify;
