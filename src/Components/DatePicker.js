import React, { useEffect } from "react";
import ReactDatePicker from "react-datepicker";
import PropTypes from "prop-types";
import moment from "moment";
import styled from "styled-components";
import Form from "react-bootstrap/Form";

import Field from "./Field";

/**
 * picker prop definition
 */
const dateProps = PropTypes.oneOfType([
  PropTypes.instanceOf(Date),
  PropTypes.string,
]);

const pickerPropTypes = {
  setFieldValue: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  format: PropTypes.string,
  label: PropTypes.string,
  minDate: dateProps,
  maxDate: dateProps,
  value: dateProps.isRequired,
};

const pickerDefaultProps = {
  value: "",
  placeholder: "",
  setFieldValue: () => {},
};

/**
 * date picker
 *
 * @param {*} name form name for datepicker
 * @param {*} label label for form input
 * @param {*} value value for form input
 * @param {*} format date format
 * @param {*} maxDate max date the picker should accept
 * @param {*} minDate min date the picker should accept
 * @param {*} placeholder self explanatory
 * @param {*} setFieldValue function to set the date in formik
 */
const Picker = ({
  name,
  label,
  value,
  format,
  maxDate,
  minDate,
  placeholder,
  setFieldValue,
  ...props
}) => {
  const date = value ? new Date(value) : "";

  return (
    <Field
      name={name}
      value={date}
      label={label}
      selected={date}
      showYearDropdown
      showMonthDropdown
      useComponent={false}
      component={ReactDatePicker}
      placeholderText={placeholder}
      onChange={(value) => setFieldValue(name, value)}
      {...(maxDate && { maxDate: moment(maxDate).unix() * 1000 })}
      {...(minDate && { minDate: moment(minDate).unix() * 1000 })}
      {...props}
    />
  );
};

Picker.propTypes = pickerPropTypes;
Picker.defaultProps = pickerDefaultProps;

/**
 * range props definition
 */
const rangePropTypes = {
  setFieldValue: PropTypes.func.isRequired,
  names: PropTypes.arrayOf(PropTypes.string).isRequired,
  values: PropTypes.arrayOf(dateProps).isRequired,
};

/**
 * range picker
 * @param {*} names array of from and to names
 * @param {*} label label of the inputs
 * @param {*} values array of from and to values
 * @param {*} minDate self explanatory
 * @param {*} maxDate self explanatory
 * @param {*} setFieldValue function to set formik value
 */
const Range = ({
  names,
  label,
  values,
  minDate,
  maxDate,
  setFieldValue,
  ...props
}) => {
  /**
   * variables
   */
  const [fromName, toName] = names;
  const [fromValue, toValue] = values;

  /**
   * effect
   */
  useEffect(() => {
    if (fromValue && toValue) {
      if (moment(fromValue).isAfter(moment(toValue))) {
        setFieldValue(toName, "");
      }
    }
  }, [values]);

  return (
    <Form.Group>
      {label && <Form.Label>{label}</Form.Label>}
      <RangeWrapper className="input-container">
        <Picker
          name={fromName}
          value={fromValue}
          minDate={minDate}
          maxDate={maxDate}
          setFieldValue={setFieldValue}
          startadornment={{
            children: <span className="text-body gull-grey-text">From</span>,
          }}
          {...props}
        />
        <Picker
          name={toName}
          value={toValue}
          maxDate={maxDate}
          minDate={fromValue}
          setFieldValue={setFieldValue}
          startadornment={{
            children: <span className="text-body gull-grey-text">To</span>,
          }}
          {...props}
        />
      </RangeWrapper>
    </Form.Group>
  );
};

Range.propTypes = rangePropTypes;

/**
 * styles
 */
const RangeWrapper = styled.div`
  grid-template-columns: 50% 50%;
  display: grid;

  .input-container {
    padding: 0px;
    border: none;
    height: 46px;
    box-shadow: none;
  }

  .form-group {
    margin-bottom: 0px;

    .form-control {
      padding-right: 0px;
    }
  }
`;

/**
 * export functions
 */
export { Range, Picker };
