import React from "react";
import { ToastContainer } from "react-toastify";
import { SWRConfig } from "swr";

import ErrorBoundary from "../Utils/ErrorBoundary";
import Http from "../Utils/Http";

const AppProvider = ({ children }) => {
  return (
    <ErrorBoundary>
      <SWRConfig
        value={{
          fetcher: (url) => Http.get(url).then(({ data }) => data),
          dedupingInterval: 1000 * 60 * 15,
          shouldRetryOnError: false,
          errorRetryInterval: 0,
          errorRetryCount: 2,
        }}
      >
        {children}
      </SWRConfig>
      <ToastContainer />
    </ErrorBoundary>
  );
};

export default AppProvider;
