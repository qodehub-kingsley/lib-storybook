# nss-react-lib

> nss react library of components

[![NPM](https://img.shields.io/npm/v/nss-react-lib.svg)](https://www.npmjs.com/package/nss-react-lib) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save nss-react-lib
```

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from 'nss-react-lib'
import 'nss-react-lib/dist/index.css'

class Example extends Component {
  render() {
    return <MyComponent />
  }
}
```

## License

MIT © [lawrenceadu](https://github.com/lawrenceadu)
