import React, { useState } from "react";

import Dropdown from "react-bootstrap/Dropdown";
import Modal from "react-bootstrap/Modal";
import Tabs from "react-bootstrap/Tabs";
import Form from "react-bootstrap/Form";
import Card from "react-bootstrap/Card";
import Tab from "react-bootstrap/Tab";
import {
  Date,
  Icon,
  Grid,
  Alert,
  Badge,
  Field,
  Button,
  Notify,
  Select,
  Upload,
  Confirm,
  Countries,
  CheckedTab,
  Instructions,
} from "nss-react-lib";
import { Formik } from "formik";
import { ToastContainer, toast } from "react-toastify";

import "nss-react-lib/dist/index.css";

const App = () => {
  const [modal, setModal] = useState(false);

  const handleToast = (type, title) => {
    return toast(
      <Notify
        body="Hello world. This is a toast"
        {...(title && { title: title })}
        type={type}
      />,
    );
  };

  return (
    <div
      className="p-md-8"
      style={{ backgroundColor: "#fff", height: "100vh" }}
    >
      <Card>
        <Card.Body>
          <h1 className="mb-8">Tabs</h1>

          <div className="nav-tabs-container hide-scroll">
            <Tabs defaultActiveKey="first" id="tabs">
              <Tab eventKey="first" title="First" />
              <Tab eventKey="second" title="Second" />
              <Tab eventKey="third" title="Third" />
              <Tab eventKey="fourth" title="Fourth" />
              <Tab eventKey="five" title="Five" />
              <Tab eventKey="six" title="Six" />
            </Tabs>
          </div>
        </Card.Body>
        <Card.Body>
          <h1 className="mb-8">Buttons</h1>

          <Grid sm="repeat(3, 1fr)">
            <Button>Button</Button>
            <Button isValid={false}>
              <Icon.ChevronLeft />
              <span className="ml-1">Button</span>
            </Button>
            <Button isSubmitting>Button</Button>

            <Button outline>Button</Button>
            <Button outline isValid={false}>
              <Icon.ChevronLeft />
              <span className="ml-1">Button</span>
            </Button>
            <Button isSubmitting outline loadingColor="var(--chateau-green)">
              Button
            </Button>

            <Button isValid round>
              <Icon.ArrowRight />
            </Button>
            <Button round isValid={false}>
              <Icon.ArrowRight />
            </Button>
            <Button isSubmitting round>
              <Icon.ArrowRight />
            </Button>

            <Button isValid round outline>
              <Icon.ArrowRight />
            </Button>
            <Button round outline isValid={false}>
              <Icon.ArrowRight />
            </Button>
            <Button
              isSubmitting
              round
              outline
              loadingColor="var(--chateau-green)"
            >
              <Icon.ArrowRight />
            </Button>
          </Grid>
        </Card.Body>
        <Card.Body>
          <h1 className="mb-8">Dropdown</h1>
          <Dropdown>
            <Dropdown.Toggle>
              <span className="mr-1">Dropdown</span>
              <Icon.ChevronDown />
            </Dropdown.Toggle>
            <Dropdown.Menu>
              <Dropdown.Item>Hello world This is really good</Dropdown.Item>
              <Dropdown.Item>Hi world</Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
        </Card.Body>
        <Card.Body>
          <h1 className="mb-8">Form</h1>
          <Formik
            initialValues={{
              hello: "",
              date: "",
              from: "",
              to: "",
              country: "",
            }}
          >
            {({ handleSubmit, values, setFieldValue, setFieldError }) => (
              <Form>
                <Form.Group>
                  <Form.Label>Hello world</Form.Label>
                  <Form.Control />
                </Form.Group>
                <Form.Group>
                  <Form.Label>Hello world</Form.Label>
                  <Form.Control
                    disabled
                    placeholder="Hello world"
                    value="Hello world"
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Label>Checkbox and Radio</Form.Label>
                  <Card>
                    <Card.Body className="p-4">
                      <Form.Check
                        custom
                        id="hello"
                        type="checkbox"
                        className="mb-4"
                        label={<span>Hello world</span>}
                      />
                      <Form.Check
                        custom
                        type="radio"
                        id="hello-1"
                        label={<span>Hello world</span>}
                      />
                    </Card.Body>
                  </Card>
                </Form.Group>

                <Field
                  name="hello"
                  component={Select}
                  placeholder="Hello"
                  value={values.hello}
                  label="Select"
                  options={[
                    {
                      label: "Hello world",
                      value: "hello",
                      isUrl: true,
                      avatar: "https://placeholder.com/150",
                    },
                  ]}
                  onChange={({ value }) => setFieldValue("hello", value)}
                />

                {/* <Field
                  isDisabled
                  name="hello"
                  value="hello"
                  component={Select}
                  placeholder="Hello"
                  label="Select Disabled"
                  options={[
                    {
                      label: "Hello world",
                      value: "hello",
                    },
                  ]}
                />

                <Date.Picker
                  name="date"
                  label="Select Date"
                  value={values.date}
                  maxDate={new window.Date()}
                  {...{ setFieldValue }}
                />
                <Date.Range
                  label="Date uploaded"
                  names={["from", "to"]}
                  values={[values.from, values.to]}
                  {...{ setFieldValue, setFieldError }}
                />

                <Field
                  name="country"
                  label="Country"
                  placeholder=""
                  component={Countries}
                  value={values.country}
                  onChange={({ value }) => setFieldValue("country", value)}
                /> */}
              </Form>
            )}
          </Formik>
        </Card.Body>
        <Card.Body>
          <h1 className="mb-8">Typography</h1>

          <div className="mb-6">
            <h3 className="mb-2">Headers</h3>
            <h1>H1</h1>
            <h2>H2</h2>
            <h3>H3</h3>
            <h4>H4</h4>
            <h5>H5</h5>
          </div>

          <div>
            <h3 className="mb-2">Others</h3>
            <h5 className="text-strike">Personal Information</h5>
          </div>
        </Card.Body>
        <Card.Body>
          <h1 className="mb-8">Icons</h1>
          <Grid sm="1fr" flow="row">
            <div>
              <h3 className="mb-2">Arrows</h3>
              <Grid sm="repeat(4, 1fr)">
                <Icon.ArrowDown />
                <Icon.ArrowLeft />
                <Icon.ArrowRight />
                <Icon.ArrowUp />
              </Grid>
            </div>
            <div>
              <h3 className="mb-2">Chevron</h3>
              <Grid sm="repeat(4, 1fr)">
                <Icon.ChevronDown title="Chevron Down" />
                <Icon.ChevronLeft />
                <Icon.ChevronRight />
                <Icon.ChevronUp />
                <Icon.ChevronDown variant="round" />
                <Icon.ChevronLeft variant="round" />
                <Icon.ChevronRight variant="round" />
                <Icon.ChevronUp variant="round" />
              </Grid>
            </div>
            <div>
              <h3 className="mb-2">Others</h3>
              <Grid sm="repeat(4, 1fr)">
                <Icon.Visibility />
                <Icon.Close />
                <Icon.Close variant="round" />
                <Icon.Print />
                <Icon.ID />
                <Icon.Upload />
                <Icon.File />
                <Icon.Mobile />
                <Icon.Bank />
                <Icon.Profile />
                <Icon.Check />
                <Icon.Search />
                <Icon.Copy />
              </Grid>
            </div>
          </Grid>
        </Card.Body>
        <Card.Body>
          <h1 className="mb-8">Checked Tabs</h1>

          <div>
            <h3 className="mb-2">Small</h3>

            <CheckedTab
              tabs={[
                { title: "Hello world" },
                { title: "Hi world" },
                { title: "Howdy world" },
              ]}
              activeKey={1}
            />

            <CheckedTab
              tabs={[
                {
                  title: "Local",
                  desc: "Graduate from an institution here in Ghana",
                },
                {
                  title: "International",
                  desc: "Graduate from an institution abroad",
                },
              ]}
              variant="lg"
              activeKey={0}
            />

            <CheckedTab
              tabs={[
                {
                  title: "Mobile money",
                  icon: Icon.Mobile,
                },
                {
                  title: "Bank",
                  icon: Icon.Bank,
                },
              ]}
              variant="icon"
              activeKey={0}
            />
          </div>
        </Card.Body>
        <Card.Body>
          <h1 className="mb-8">Modal and Confirmation</h1>

          <Button onClick={() => setModal(true)} className="mb-8">
            Open Modal
          </Button>
          <Button
            onClick={async () => {
              if (
                await Confirm({ msg: "Hello world", header: "Hello world" })
              ) {
              }
            }}
            className="mb-8"
          >
            Open Confirm
          </Button>
          <Button
            onClick={() => toast(<Notify body="Hello world" type="error" />)}
            className="mb-8"
          >
            Open Toast
          </Button>

          <Dropdown>
            <Dropdown.Toggle className="btn--default">
              <span className="mr-2">Open toast</span>
              <Icon.ChevronDown />
            </Dropdown.Toggle>
            <Dropdown.Menu>
              <Dropdown.Item onClick={() => handleToast("success")}>
                Success toast
              </Dropdown.Item>
              <Dropdown.Item onClick={() => handleToast("error")}>
                Error toast
              </Dropdown.Item>
              <Dropdown.Item onClick={() => handleToast(null)}>
                Default toast
              </Dropdown.Item>
              <Dropdown.Item onClick={() => handleToast(null, "Header")}>
                With header
              </Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>

          <Modal show={modal} onHide={() => setModal(false)} centered>
            <Modal.Body>
              <p className="text-body">Hello world</p>
            </Modal.Body>
          </Modal>
        </Card.Body>
        <Card.Body>
          <h1 className="mb-8">Badges</h1>

          <div className="mb-4">
            <h3 className="mb-3">Default badges</h3>
            <div className="d-flex mx-n2">
              <Badge variant="failed" className="mx-2">
                Failed
              </Badge>
              <Badge variant="success" className="mx-2">
                Success
              </Badge>
              <Badge variant="processing" className="mx-2">
                Processing
              </Badge>
            </div>
          </div>
          <div className="mb-0">
            <h3 className="mb-3">Other badges</h3>
            <div className="d-flex mx-n2">
              <Badge bg="var(--casablanca)" className="mx-2">
                Warning
              </Badge>
              <Badge bg="var(--gull-grey)" className="mx-2">
                Unknown
              </Badge>
              <Badge
                bg="var(--gin)"
                color="var(--black-pearl)"
                className="mx-2"
              >
                Processing
              </Badge>
            </div>
          </div>
        </Card.Body>

        <Card.Body>
          <h1 className="mb-8">Uploads</h1>

          <Upload
            value={{ name: "CTU - Derrick.pdf" }}
            label="Upload document"
            className="mb-4"
          />
          <Upload
            value={{ name: "CTU - Derrick Ablorh Transcript.pdf" }}
            setFieldValue={(file) => console.log(file)}
            label="Upload document"
            accept="image/*"
            className="mb-4"
            withClear
            max={2}
          />
          <Upload
            setFieldValue={(file) => console.log(file)}
            label="Upload document"
            accept="image/*"
            value=""
            max={2}
          />
          <Upload
            tip="Some little information on what other document specifically entails"
            setFieldValue={(file) => console.log(file)}
            label="Upload document"
            accept="image/*"
            value=""
            max={2}
          />
        </Card.Body>
        <Card.Body>
          <h1 className="mb-8">Instructions</h1>

          <Instructions
            instructions={[
              "Only Ghanaian nationals can apply for national service.",
              "Graduates from Ghanaian universities MUST have graduated at least 1 year prior to the service year they seek to join.",
              "All current year graduates can only enroll for national service through their institution of study.",
              "International graduates must have fully completed their studies, with results published to enroll for NSS.",
            ]}
          />
        </Card.Body>

        <Card.Body>
          <h1 className="mb-8">Alerts</h1>
          <Grid gap="24px" sm="1fr">
            <Alert variant="primary">Hello world</Alert>
            <Alert variant="primary">
              <Icon.Info variant="fill" />
              Hello world
            </Alert>
            <Alert variant="primary" withBorder>
              <Icon.Info variant="fill" />
              Hello world
            </Alert>
            <Alert variant="success">Hello world</Alert>
            <Alert variant="success">
              <Icon.Info variant="fill" />
              Hello world
            </Alert>
            <Alert variant="success" withBorder>
              <Icon.Info variant="fill" />
              Hello world
            </Alert>
            <Alert variant="warning">Hello world</Alert>
            <Alert variant="warning">
              <Icon.Info variant="fill" />
              Hello world
            </Alert>
            <Alert variant="warning" withBorder>
              <Icon.Info variant="fill" />
              Hello world
            </Alert>
            <Alert variant="danger">Hello world</Alert>
            <Alert variant="danger">
              <Icon.Info variant="fill" />
              Hello world
            </Alert>
            <Alert variant="danger" withBorder>
              <Icon.Info variant="fill" />
              Hello world
            </Alert>
            <Alert color="var(--gull-grey)">Hello world</Alert>
            <Alert color="var(--gull-grey)">
              <Icon.Info variant="fill" />
              Hello world
            </Alert>
            <Alert color="var(--gull-grey)" withBorder>
              <Icon.Info variant="fill" />
              Lorem ipsum dolor sit amet consectetur adipisicing elit.
              Reprehenderit aspernatur, id atque deserunt repellendus minus
              ullam possimus amet iure, voluptatibus itaque fugit. Id
              repellendus eveniet vel dolor perspiciatis, assumenda quibusdam?
            </Alert>
          </Grid>
        </Card.Body>
      </Card>

      <ToastContainer />
    </div>
  );
};

export default App;
